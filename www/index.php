<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use Illuminate\Database\Capsule\Manager as DB;
/**
 * Autoloader
 */
require_once '../src/vendor/autoload.php';

use games\model\Character;
use games\model\Company;
use games\model\GameRating;
use games\model\Game;
use games\model\Genre;
use games\model\Platform;
use games\model\RatingBoard;
use games\model\Theme;

/**
 * Création connection avec la DB
 */
new games\db\Connection();

$app = new \Slim\App([
    'settings' => [
        'displayErrorDetails' => true
    ]
]);

//Affichage de l'index
$app->get('/',
        function (Request $req, Response $resp, $args) {
            $res = '<h1 id="haut">Partie 1</h1><a href="#Q1">lister les jeux dont le nom contient "Mario" </a><br>
            <a href="#Q2">lister les compagnies installées au Japon</a><br>
            <a href="#Q3">lister les plateformes dont la base installée est >= 10 000 000</a><br>
            <a href="#Q4">lister 442 jeux à partir du 21173ème</a><br><br><h1>Partie 2</h1><br>
            <a href="#Q5">afficher (name , deck) les personnages du jeu 12342</a><br>
            <a href="#Q6">les personnages des jeux dont le nom (du jeu) débute par "Mario"</a><br>
            <a href="#Q7">les jeux développés par une compagnie dont le nom contient "Sony"</a><br>
            <a href="#Q8">le rating initial (indiquer le rating board) des jeux dont le nom contient Mario</a><br>
            <a href="#Q9">les jeux dont le nom débute par Mario et ayant plus de 3 personnages</a><br>
            <a href="#Q10">les jeux dont le nom débute par Mario et dont le rating initial contient "3+"</a><br>
            <a href="#Q11">les jeux dont le nom débute par Mario, publiés par une compagnie dont le nom contient "Inc." et dont le rating initial contient "3+"</a><br>
            <a href="#Q12">les jeux dont le nom débute Mario, publiés par une compagnie dont le nom contient "Inc", dont le rating initial contient "3+" et ayant reçu un avis de la part du rating board nommé "CERO"</a><br>' ;

            DB::connection()->enableQueryLog();
            $res .='<br><h2 id="Q1">Jeux content "mario" </h2><br><ul><a href="#haut"><h3>revenir en haut</h3></a><br>';
            $t = microtime(true);
            foreach (Game::where('name', 'like', 'Mario%')->get() as $game) {
                $res.= '<li>---- '. $game->name . ' : ' . $game->id . "</li>\n";
            }
            $t2 = microtime(true);
            $t2 = $t2 -$t;
            $res .= "<h5>Temps d'execution: ".$t2." ms</h5>";
            foreach( DB::getQueryLog() as $log){
                $res .= "<h5>-------------- </h5>\n";
                $res .= "<h5>query : " . $log['query'] ."</h5>\n";
                $res .= " <h5>--- bindings : [ </h5>";
                foreach ($log['bindings'] as $b ) {
                    $res .= "<h5> ". $b.",</h5>" ;
                }
            }
            $res.="</ul>";

            $res .='<h2 id="Q2">Company Japonaise </h2><br><ul><a href="#haut"><h3>revenir en haut</h3></a><br>';
            $t = microtime(true);
            foreach (Company::where('location_country', 'like', '%Japan%')->get() as $compJapan) {
                $res.= '<li>---- '. $compJapan->name . ' : ' . $compJapan->id . "</li>\n";
            }
            $t2 = microtime(true)-$t;
            $res .= "<h5>Temps d'execution: ".$t2." ms</h5>";
            $res.="</ul>";

            $res .='<h2 id="Q3">Platform Install Base >= 10 000 000</h2><br><ul><a href="#haut"><h3>revenir en haut</h3></a><br>';
            $t = microtime(true);
            foreach (Platform::where('install_base', '>=', 10000000)->get() as $platinstalbase) {
                $res.= '<li>---- '. $platinstalbase->name . ' : ' . $platinstalbase->id . "</li>\n";
            }
            $t2 = microtime(true)-$t;
            $res .= "<h5>Temps d'execution: ".$t2." ms</h5>";
            $res.="</ul>";

            $res .='<h2 id="Q4">442 Jeux</h2><br><ul><a href="#haut"><h3>revenir en haut</h3></a><br>';
            $t = microtime(true);
            foreach (Game::where([['id', '>=', 21173],['id', '<',21615]])->get() as $gameSupId) {
                $res.= '<li>---- '. $gameSupId->name . ' : ' . $gameSupId->id . "</li>\n";
            }
            $t2 = microtime(true)-$t;
            $res .= "<h5>Temps d'execution: ".$t2." ms</h5>";
            $res.="</ul>";

            $res.='<br>';
            $res .='<h2 id="Q5">Character du jeux 12342</h2><br><a href="#haut"><h3>revenir en haut</h3></a><br><ul>';
            $t = microtime(true);
               foreach (Game::where('id', '=', 12342)->get() as $game) {
                $res .= "<h4>".$game->name . " :</h4>";
                foreach ($game->characters as $ch) {
            
                    $res .= '<li>---- '.$ch->id . '. ' . $ch->name . ' : '.$ch->deck . "</li>\n" ;
            
                }
            }
            $t2 = microtime(true)-$t;
            $res .= "<h5>Temps d'execution: ".$t2." ms</h5>";
            foreach( DB::getQueryLog() as $log){
                $res .= "<h5>-------------- </h5>\n";
                $res .= "<h5>query : " . $log['query'] ."</h5>\n";
                $res .= " <h5>--- bindings : [ </h5>";
                foreach ($log['bindings'] as $b ) {
                    $res .= "<h5> ". $b.",</h5>" ;
                }
            }
            $res.="</ul>";
            $res .='<h2 id="Q6">Character des jeux dont le nom débute par Mario</h2><br><a href="#haut"><h3>revenir en haut</h3></a><br><ul>';
            $t = microtime(true);
            foreach (Game::where('name', 'like', 'Mario%')->get() as $game) {
                $res.="<h4>". $game->name . " :</h4>\n";
                foreach ($game->characters as $ch) {
            
                    $res.= '<li>--- '.$ch->id . '. ' . $ch->name . ' : '.$ch->deck . "</li>\n" ;
            
                }
            }
            $t2 = microtime(true)-$t;
            $res .= "<h5>Temps d'execution: ".$t2." ms</h5>";
            foreach( DB::getQueryLog() as $log){
                $res .= "<h5>-------------- </h5>\n";
                $res .= "<h5>query : " . $log['query'] ."</h5>\n";
                $res .= " <h5>--- bindings : [ </h5>";
                foreach ($log['bindings'] as $b ) {
                    $res .= "<h5> ". $b.",</h5>" ;
                }
            }
            $res.="</ul>";
            $res .='<h2 id="Q7">Jeux fait la compagnie sony</h2><br><a href="#haut"><h3>revenir en haut</h3></a><br><ul>';
            foreach (Company::where('name', 'like', '%Sony%')->get() as $sony) {
                $res.="<h4>". $sony->name . " :</h4>\n";
                foreach ($sony->games as $ch) {
            
                    $res.= '<li>--- '.$ch->id . '. ' . $ch->name . ' : '.$ch->deck . "</li>\n" ;
            
                }
            }
            foreach( DB::getQueryLog() as $log){
                $res .= "<h5>-------------- </h5>\n";
                $res .= "<h5>query : " . $log['query'] ."</h5>\n";
                $res .= " <h5>--- bindings : [ </h5>";
                foreach ($log['bindings'] as $b ) {
                    $res .= "<h5> ". $b.",</h5>" ;
                }
            }
            $res.="</ul>";
            $res .='<h2 id="Q8">Rating des jeux mario</h2><br><a href="#haut"><h3>revenir en haut</h3></a><br><ul>';
            $t = microtime(true);
            foreach (Game::where('name', 'like', 'Mario%')->get() as $game) {
                $res.= '<h4>---- '. $game->name . ' : ' . $game->id . "</h4>\n";
                        foreach ($game->original_game_ratings as $rating) {
                            $res.= '<li>####### '. $rating->name . ' ('. $rating->rating_board->name . ")</li>\n";
                        }
            }
            $t2 = microtime(true)-$t;
            $res .= "<h5>Temps d'execution: ".$t2." ms</h5>";
            $res.="</ul>";


            $res .='<h2 id="Q9">Liste des characters des jeux mario où il y a plus de 3 personnages</h2><br><a href="#haut"><h3>revenir en haut</h3></a><br><ul>';
            $t = microtime(true);
                    foreach (Game::where('name', 'like', 'Mario%')->has('characters', '>', 3)->get() as $game) {
                        $res.= '<h4>'.$game->name . ' : ' . $game->id . "</h4>\n";
                        foreach ($game->characters as $ch) {
                    
                            $res.= '<li>--- '.$ch->id . '. ' . $ch->name . ' : '.$ch->deck . "\n" ;
                    
                        }
                    
                    }
                    $t2 = microtime(true)-$t;
                    $res .= "<h5>Temps d'execution: ".$t2." ms</h5>";

            $res .='<h2 id="Q10">Liste des characters des jeux mario si ils sont plus de 3</h2><br><a href="#haut"><h3>revenir en haut</h3></a><br><ul>';
            $t = microtime(true);
            foreach (Game::where('name', 'like', 'Mario%')->whereHas('original_game_ratings', function($q){
                $q->where('name', 'like', '%3+%');})
                ->get() as $game)  {
                    $res.= '<h4>#### '. $game->name . ' : ' . $game->id . "\n";
                foreach ($game->original_game_ratings as $rating) {
                    $res.= '<li>-------- '. $rating->name .  "</li>\n";
                }
                foreach ($game->publishers as $comp) {
                    $res.= '<li>-------- '. $comp->name .  "</li>\n";
                }

            }
            $t2 = microtime(true)-$t;
            $res .= "<h5>Temps d'execution: ".$t2." ms</h5>";
            $res.="</ul>";


            $res .='<h2 id="Q11">les jeux dont le nom débute par Mario, publiés par une compagnie dont le nom contient
            "Inc." et dont le rating initial contient "3+"</h2><br><a href="#haut"><h3>revenir en haut</h3></a><br><ul>';
            $t = microtime(true);
            foreach (Game::where('name', 'like', 'Mario%')
            ->whereHas('original_game_ratings', function($q){
                $q->where('name', 'like', '%3+%');
            })
            ->whereHas('publishers', function($q) {
                $q->where('name', 'like', '%Inc.%');
            })->get() as $game) {
                 $res.= '<h4>#### ' . $game->name . ' : ' . $game->id . "</h4>\n";
            foreach ($game->original_game_ratings as $rating) {
                $res.= '<li>-------- ' . $rating->name . "</li>\n";
            }
            foreach ($game->publishers as $comp) {
                $res.= '<li>--> publisher : '. $comp->name .  "</li>\n";
            }
            }
            $t2 = microtime(true)-$t;
            $res .= "<h5>Temps d'execution: ".$t2." ms</h5>";
            $res.="</ul>";
            $res .='<h2 id="Q12">les jeux dont le nom débute Mario, publiés par une compagnie dont le nom contient "Inc",
            dont le rating initial contient "3+" et ayant reçu un avis de la part du rating board nommé
            "CERO"</h2><br><a href="#haut"><h3>revenir en haut</h3></a><br><ul>';
            $t = microtime(true);
                foreach (Game::where('name', 'like', 'Mario%')
                            ->whereHas('original_game_ratings', function($q){
                                $q->where('name', 'like', '%3+%');
                            })->whereHas('original_game_ratings.rating_board', function($q){
                                $q->where('name', '=', 'CERO');
                            })->whereHas('publishers', function($q) {
                                $q->where('name', 'like', '%Inc.%');
                            })->get() as $game) {
                        $res.= '<h4>#### ' . $game->name . ' : ' . $game->id . "</h4>\n";
                    foreach ($game->original_game_ratings as $rating) {
                        $res.= '<li>-------- ' . $rating->name . "</li>\n";
                    }
                    foreach ($game->publishers as $comp) {
                        $res.= '<li>--> publisher : '. $comp->name .  "</li>\n";
                    }
                }
                $t2 = microtime(true)-$t;
                $res .= "<h5>Temps d'execution: ".$t2." ms</h5>";
                $res.="</ul>";
             return $resp->getBody()->write($res);
            
        }
)->setName('index');

$app->run();