<?php

namespace games\db;
use Illuminate\Database\Capsule\Manager as DB;
class Connection
{
    public function __construct()
    {
        $db = new DB();
        $db->addConnection(parse_ini_file('../src/conf/conf.ini'));
        $db->setAsGlobal();
        $db->bootEloquent();
    }
}