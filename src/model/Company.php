<?php

namespace games\model;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public $timestamps = false;

    protected $table = 'company';
    protected $primaryKey = 'id';
    protected $location_country= 'location_country';

    public function games() {

        return $this->belongsToMany('\games\model\Game', 'game_publishers', 'comp_id', 'game_id');
    }
}