<?php

namespace games\model;

class Platform extends \Illuminate\Database\Eloquent\Model {
    protected $table='platform';
    protected $primaryKey='id';

    public function company() {
        return $this->belongsTo('bd\modele\company\id','lieu_id');
        }

    public function game() {
        return $this->belongsTo('bd\modele\company\id');
    }


}
