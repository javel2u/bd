<?php

namespace games\model;

use Illuminate\Database\Eloquent\Model;

class RatingBoard extends Model
{
    public $timestamps = false;
    protected $table = 'rating_board';
    protected $primaryKey = 'id';
     

}